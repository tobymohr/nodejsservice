import express from 'express';
import { studentRouter } from '../routes/studentRoute';
import { students } from '../apiControllers/studentController';
import * as MediaContentRoute from '../routes/mediaContentRoute';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import path from 'path';
import fs from 'fs';
import swaggerDoc from '../swaggerDoc';
import cors from 'cors';

/** URLS and Port */
const buildUrl = (version, path) => `/api/${version}/${path}`;

const PORT = 3000;
const ENV = process.env.NODE_ENV;
const STUDENTS_BASE_URL = buildUrl('v1', 'students');
const MEDIA_BASE_URL = buildUrl('v1', 'download/images');

const server = express();

/** Cors */
server.use(cors());

/** Setup of server */
MediaContentRoute.setupMediaRoutes(server);
server.use(bodyParser.json());
server.use(STUDENTS_BASE_URL, studentRouter);
server.use(MEDIA_BASE_URL, MediaContentRoute.mediaRouter);

/** Setup of logging */
const logger = morgan('common', {
	stream: fs.createWriteStream('./access.log', { interval: '1d', flags: 'a' })
});
server.use(logger);

/** Swagger */
swaggerDoc(server);

/**
 * Starts the server
 */
const startServer = () =>
	server.listen(PORT, () => {
		console.log(`--- Server is started on port ${PORT} ---`);
		if (ENV) {
			console.log(`--- Environment: ${process.env.NODE_ENV} ---`);
		} else {
			console.log('--- Environment not set. Default environment is LOCAL ---');
		}
	});

const serverObject = {
	startServer
};

export default serverObject;
