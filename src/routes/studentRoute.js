import express from 'express';
import * as studentController from '../apiControllers/studentController';

const studentRouter = express.Router();

/**
 * @swagger
 * definitions:
 *   Student:
 *     properties:
 *       id:
 *         type: string
 *       name:
 *         type: string
 *       text:
 *         type: string
 *       profilePicturePath:
 *         type: string
 *   Students: 
 *     type: array
 *     items:
 *       $ref: '#/definitions/Student'
 */

/**
 * @swagger
 *  students:
 *    get:
 *      tags:
 *          - Students
 *      summary: This should return all students
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: A json array containing all students.
 *          schema:
 *             "$ref": '#/definitions/Students'
 */
studentRouter.get('/', studentController.getAllStudents);

studentRouter.get('/images', studentController.getAllStudentImages);

/**
 * @swagger
 * /students/{id}:
 *    get:
 *      tags:
 *          - Students
 *      parameters:
 *        - in: path
 *          name: id
 *          type: string
 *          required: true
 *      summary: This should return the specified student
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: A json object containing the specific student.
 *          schema:
 *             type: object
 *             $ref: '#/definitions/Student'
 * 
 */
studentRouter.get('/:id', studentController.getStudentById);

/**
 * @swagger
 * /students:
 *    post:
 *      summary: Create a new student
 *      tags: 
 *          - Students
 *      parameters:
 *        - in: body
 *          name: Body
 *          type: string
 *          required: true
 *          schema:
 *            "$ref": '#/definitions/Student'
 *      responses:
 *        "200":
 *          description: A student schema
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/definitions/Student'
 */
studentRouter.post('/', studentController.createStudent);

/**
 * @swagger
 * /students/{id}:
 *    put:
 *      tags:
 *          - Students
 *      parameters:
 *        - in: path
 *          name: id
 *          type: string
 *          required: true
 *        - in: body
 *          name: Body
 *          type: string
 *          required: true
 *          schema:
 *            "$ref": '#/definitions/Student'
 *      summary: This should return the updated student
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: A json object containing the specific student.
 *          schema:
 *             type: object
 *             $ref: '#/definitions/Student'
 */
studentRouter.put('/:id', studentController.updateStudent);

/**
 * @swagger
 * /students/{id}:
 *    delete:
 *      tags:
 *          - Students
 *      parameters:
 *        - in: path
 *          name: id
 *          type: string
 *          required: true
 *      summary: This should delete the specified student
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: A message saying that the student was deleted.
 *          schema:
 *             type: object
 *             $ref: '#/definitions/Student'
 */
studentRouter.delete('/:id', studentController.deleteStudent);

module.exports.studentRouter = studentRouter;