import express from 'express';
import path from 'path';

const mediaRouter = express.Router();

const setupMediaRoutes = (server) => {
	server.use(express.static('public'));
};

mediaRouter.get('/:imageName', (req,res)=> {
	res.download(path.join('public','images',req.params.imageName));
});

module.exports.mediaRouter = mediaRouter;
module.exports.setupMediaRoutes = setupMediaRoutes;