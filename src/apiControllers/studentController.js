import mongoose from 'mongoose';
import database from '../database/context';
import StudentSchemaObject from '../database/schemas/studentSchema';
import fetch from 'node-fetch';
database.connect();

exports.getStudentById = async (req, res) => {
	const id = req.params.id;
	try{
		const studentWithId = await StudentSchemaObject.StudentModel.findById(id);
		if(studentWithId){
			res.status(200).json(studentWithId);
		} else {
			res.status(404).send(`No student with matching id: ${id}`);
		}
	}catch(err){
		res.status(500).json(err);
	}
};

exports.getAllStudents = async (req, res) => {
	const students = await StudentSchemaObject.StudentModel.find();
	res.status(200).json(students);
};

exports.getAllStudentImages = async (req, response) => {
	try {
		const res = await fetch('http://golangapi:8080/v1/api/images');
		response.status(200).send(await res.text());
	} catch (err){
		response.status(500).send('Error');
	}
};

exports.createStudent = async (req, res) => {
	const body = req.body;
	const id = new mongoose.Types.ObjectId();
	const studentRaw = Object.assign({ _id: id }, body);
	const studentToSave = new StudentSchemaObject.StudentModel(studentRaw);
	try{
		const student = await studentToSave.save();
		res.status(200).json(student);
	}catch(err){
		res.status(500).json(err);
	}
};

exports.updateStudent = async (req, res) => {
	const id = req.params.id;
	try{
		const studentWithId = await StudentSchemaObject.StudentModel.findById(id);
		if(studentWithId){
			studentWithId.name = req.body.name;
			studentWithId.course = req.body.course;
			const savedStudent = await studentWithId.save();
			res.status(200).json(savedStudent);
		} else {
			res.status(404).send(`No student matching ${id}`);
		}

	}catch(err){
		res.status(500).json(err);
	}
};

exports.deleteStudent = async (req, res) => {
	const id = req.params.id;
	try{
		await StudentSchemaObject.StudentModel.findByIdAndRemove(id);
		res.status(200).send(`Student with id ${id} is deleted`);
	}catch(err){
		res.status(500).json(err);
	}
};

