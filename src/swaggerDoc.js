import swaggerJsdoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';

const options = {
	swaggerDefinition: {
		components: {},
		info: {
			title: 'Node JS API',
			version: '1.0.0',
			description: 'Node JS API with swagger doc',
		},
		basePath: '/api/v1/'
	},
	apis: ['src/routes/*.js'],
};

const specs = swaggerJsdoc(options);

module.exports = (app) => {
	app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(specs));
};