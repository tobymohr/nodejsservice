import  mongoose, { Schema } from 'mongoose';

const StudentSchema = new Schema({
	_id: Schema.Types.ObjectId,
	name: String,
	text: String,
	profilePicturePath: String
});

const StudentModel =  mongoose.model('Student', StudentSchema);

const StudentSchemaObject = {
	StudentSchema,
	StudentModel
};

export default StudentSchemaObject;
