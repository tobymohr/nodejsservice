import mongoose from 'mongoose';

const DB_USER = process.env.DB_USER;
const DB_USER_PASSWORD = process.env.DB_USER_PASSWORD;
const DB_URL = `mongodb+srv://${DB_USER}:${DB_USER_PASSWORD}@cluster0-hwfjk.mongodb.net/test?retryWrites=true&w=majority`;

const connect = () => mongoose.connect(DB_URL, {useNewUrlParser: true,  useUnifiedTopology: true });
const db = mongoose.connection;
db.once('open', () => {
	console.log('--- Connection to MongoDB open ---');
});

const databaseObject = {
	connect
};

export default databaseObject;