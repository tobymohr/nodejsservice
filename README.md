# NodeJsService

## To build and run development Docker container

```
docker-compose up -d --build
```

It will be accessible on localhost:3000

## To build and run production Docker container

```
docker-compose -f docker-compose-prod.yml up -d --build
```

It will be accessible on localhost:8081
